class RPNCalculator
  # TODO: your code goes here!
  attr_accessor :operands

  attr_reader :operands

  def initialize
    @operands = []
  end

  def show_operands
    @operands
  end

  def push(int)
    @operands.push(int)
  end

  def plus
    check_if_empty
    @operands.push(:+)
  end

  def minus
    check_if_empty
    @operands.push(:-)
  end

  def times
    check_if_empty
    @operands.push(:*)
  end

  def divide
    check_if_empty
    @operands.push(:/)
  end

  def value
    check_if_empty

    symbols = [:+,:-,:*,:/]
    numbers = []
    idx = 0

    @operands.each do |operand|

      unless symbols.include?(operand)
        numbers.push(operand)

      else
        case operand
          when :+
            result = numbers[-2] + numbers[-1]

          when :-
            result = numbers[-2] - numbers[-1]

          when :*
            result = numbers[-2] * numbers[-1]

          when :/
            result = numbers[-2].to_f / numbers[-1]
        end
        update_numbers(numbers,result)
      end
    end
    numbers.last
  end

  def update_numbers(numbers,result)
    numbers.pop
    numbers.pop
    numbers.push(result)
  end

  def check_if_empty
    raise "calculator is empty" if @operands.length <= 1
  end

  def tokens(string)
    string.split.each do |ch|
      unless '+-/*'.include?(ch)
        #convert to intger and push
        self.push(ch.to_i)
      else
        #convert to symbol and push
        case ch
          when "+"
            self.plus

          when "-"
            self.minus

          when "*"
            self.times

          when "/"
            self.divide

        end
      end
    end
    @operands
  end

  def evaluate(string)
    tokens(string)
    self.value
  end


end
